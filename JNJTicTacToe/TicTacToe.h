#pragma once


class TicTacToe {


private:

	//An array used to store the spaces of the game board. Once a player has Moved in one of the positions, the space should change to an X or an O.
	char m_board[10];

	//This will be used to detect when the board is full (all nine spaces are taken).
	int m_numTurns;

	//This is used to track if it's X's turn or O's turn.
	char m_playerTurn;

	//This is used to check to see the winner of the game. A space should be used while the game is being played.
	char m_winner;


public:

//Constructor
	TicTacToe() {

		SetNumTurns(0);
		SetWinner('D');
		//Set Default Blank Board
		for (int i = 1; i < 10; i++) {
			SetBoardPosition(i, ' ');
		}
		//Need final position in char array to be \0
		SetBoardPosition(10, '\0');
	}

// Mutators
	void SetNumTurns(int numTurns) { m_numTurns = numTurns; }
	void SetWinner(char winner) { m_winner = winner; }
	void SetBoardPosition(int position, char player) { m_board[position - 1] = player; }

// Accessors
	int GetNumTurns() { return m_numTurns; }
	char GetWinner() { return m_winner; }
	char GetBoardPosition(int position) { return  m_board[position-1]; };


//This method will output the board to the console.
	void DisplayBoard() {

		for (int i = 1; i < 10; i++) {
			std::cout << "|" << GetBoardPosition(i);
			//Add end of row breaks
			if (i == 3 || i == 6 || i == 9) {
				std::cout << "|" << "\n";
			}
		}
	}

//Returns true if the game is over (winner or tie), otherwise false if the game is still being played.
	bool IsOver() {

		//Decide if checking for X or O winner
		char checkWin;
		if (GetNumTurns() % 2 == 1) {
			checkWin = 'X';
		}
		else checkWin = 'O';

		//Check for draw
		if (GetNumTurns() == 9) {
			//Sequence to test for a draw: 1 3 2 4 6 5 7 8 9
			return true;
		}
		else if (
			//Rows
			(GetBoardPosition(1) == checkWin && GetBoardPosition(1) == GetBoardPosition(2) && GetBoardPosition(2) == GetBoardPosition(3)) ||
			(GetBoardPosition(4) == checkWin && GetBoardPosition(4) == GetBoardPosition(5) && GetBoardPosition(5) == GetBoardPosition(6)) ||
			(GetBoardPosition(7) == checkWin && GetBoardPosition(7) == GetBoardPosition(8) && GetBoardPosition(8) == GetBoardPosition(9)) ||
			//Columns
			(GetBoardPosition(1) == checkWin && GetBoardPosition(1) == GetBoardPosition(4) && GetBoardPosition(4) == GetBoardPosition(7)) ||
			(GetBoardPosition(2) == checkWin && GetBoardPosition(2) == GetBoardPosition(5) && GetBoardPosition(5) == GetBoardPosition(8)) ||
			(GetBoardPosition(3) == checkWin && GetBoardPosition(3) == GetBoardPosition(6) && GetBoardPosition(6) == GetBoardPosition(9)) ||
			//Diagonals
			(GetBoardPosition(1) == checkWin && GetBoardPosition(1) == GetBoardPosition(5) && GetBoardPosition(5) == GetBoardPosition(9)) ||
			(GetBoardPosition(3) == checkWin && GetBoardPosition(3) == GetBoardPosition(5) && GetBoardPosition(5) == GetBoardPosition(7))) {
			
			//Set X as winner
			SetWinner(checkWin);

			//True, game is over
			return true;
		}
		else return false;
	}


	//Returns an 'X' or an 'O,' depending on which player's turn it is.
	char GetPlayerTurn() {	
		if (GetNumTurns() % 2 == 1) {
			return 'O';
		}
		else return 'X';
	}


	//Param: An int (1 - 9) representing a square on the board. Returns true if the space on the board is open, otherwise false.
	bool IsValidMove(int position) {
		if (position > 9) {
			std::cout << "Number is not between 1 and 9. Please try again. \n";
			return false;
		}
		//If true, do move
		else if (GetBoardPosition(position) == 'X' || GetBoardPosition(position) == 'O') {
			std::cout << "Invalid position. Try again \n";
			return false;
		}
		else return true;	
	}


	//Param: int(1 - 9) representing a square on the board.Places the current players character in the specified position on the board.
	void Move(int position) {	
		if (IsValidMove(position) == true) {
			//Set board position
			SetBoardPosition(position, GetPlayerTurn());
			//Increment the turn number
			SetNumTurns(GetNumTurns() + 1);
		}
		else {
			std::cout << "Not a valid move.";
		}
	}

	//Displays a message stating who the winning player is, or if the game ended in a tie.
	void DisplayResult() {
		//Check for draw
		if (GetWinner() == 'D') {
			std::cout << "This game is a draw. \n";
		}
		else {
		std::cout << "The winner is " << GetWinner() << "\n";
		}
	}
};


